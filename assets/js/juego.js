/* 
2C = Two of Clubs
2D = Two of Diamonds 
2H = Two of Hearts 
2S = Two of Spades  
*/

(() => {
    'use strict'
    
    let deck = [];
    const tipos      = ['C','D','H','S'],
        especiales = ['A','J','Q','K'];
        
    let puntosJugador = 0,
        puntosComputadora = 0;

    // Referencias del HTML
    const btnPedir = document.querySelector('#btnPedir');
    const btnDetener = document.querySelector('#btnDetener');
    const btnNuevo = document.querySelector('#btnNuevo');

    const divCartasJugador = document.querySelector('#jugador-cartas');
    const divCartasComputadora = document.querySelector('#computadora-cartas')
    const puntosHTML = document.querySelectorAll('small');


    const crearDeck = () => {
        for ( let i = 2; i <= 10; i++ ) {
            for (const tipo of tipos) {
                deck.push(i + tipo);
            }
        } 

        for ( let tipo of tipos ) {
        for ( let esp of especiales ) {
            deck.push( esp + tipo );
        } 
        }

        deck = _.shuffle(deck);
        return deck;
    }


    crearDeck();


    // Esta función me permite tomar una carta
    const pedirCarta = () => {
        if (deck.length === 0) {
            throw 'No hay cartas en el deck'
        }

        const card = deck.pop();
        return card;
    }

    // for (let i in deck){
        // pedirCarta();
    // }

    const valorCarta = (carta) => {
        const valor = carta.substring(0, carta.length - 1);
        // let puntos  = 0;
        // if (isNaN(valor)) {
        //     puntos = ( valor === 'A' ) ? 11 : 10;
        // } else {
        //     puntos = Number(valor);
        // }}

        return ( isNaN( valor ) ) ?
            ( valor === 'A' ) ? 11 : 10 
            : Number(valor);
    }


    // const valor = valorCarta( pedirCarta() );

    // Turno Computadora
    const turnoComputadora = ( puntosMinimo ) => {
        do {
            if (puntosMinimo > 21) {
                break;
            }

            const carta = pedirCarta();
            
            puntosComputadora += valorCarta( carta );
            puntosHTML[1].innerText = puntosComputadora;
        
            const imgCarta = document.createElement('img');
            imgCarta.src = `assets/cartas/${ carta }.png`;
            imgCarta.classList.add('carta');
            divCartasComputadora.append( imgCarta );

            
        } while ( (puntosComputadora <= puntosMinimo) && (puntosMinimo <= 21));

        setTimeout(() => {
            if ( puntosComputadora === puntosJugador ) {
                alert('Nadie gana');
            } else if ( puntosMinimo > 21) {
                alert('Computadora gana');
            } else if ( puntosComputadora > 21) {
                alert('Jugador gana')
            } else {
                alert('computadora gana')
            }
        }, 50 );
        
    }


    // Eventos
    btnPedir.addEventListener('click', () => {
        const carta = pedirCarta();
        puntosJugador += valorCarta(carta);
        puntosHTML[0].innerText = puntosJugador;

        const imgCarta = document.createElement('img')
        imgCarta.src = `assets/cartas/${carta}.png`;
        imgCarta.classList.add('carta');
        divCartasJugador.append(imgCarta);

        if (puntosJugador > 21) {
            turnoComputadora(puntosJugador)
        }
    })


    btnDetener.addEventListener('click', () => {
        btnPedir.disabled = 'true';

        turnoComputadora(puntosJugador);
    })


    btnNuevo.addEventListener('click', () => {
        // window.location.reload()
        deck = [];
        deck = crearDeck();

        puntosJugador = 0;
        puntosComputadora = 0;

        puntosHTML[0].innerText = 0;
        puntosHTML[1].innerText = 0;

        divCartasComputadora.innerHTML = '';
        divCartasJugador.innerHTML = '';

        btnPedir.disabled   = false;
        btnDetener.disabled = false;
    })
})();

